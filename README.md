# Drupal 8 CI demo

This repository serves as a demo for different CI integrations that use the installation
scripts from https://github.com/lullabot/drupal8ci.

If you want to test and individual module instead of a Drupal project, check out
[drupal_tests](https://github.com/deviantintegral/drupal_tests).

## CircleCI

[![CircleCI](https://circleci.com/gh/juampynr/d8cidemo.svg?style=svg)](https://circleci.com/gh/juampynr/d8cidemo)

Test runs at https://circleci.com/gh/juampynr/d8cidemo.

## Travis

[![Build Status](https://travis-ci.org/juampynr/d8cidemo.svg?branch=master)](https://travis-ci.org/juampynr/d8cidemo)
[![Coverage Status](https://coveralls.io/repos/github/juampynr/d8cidemo/badge.svg)](https://coveralls.io/github/juampynr/d8cidemo)

Test runs at  https://travis-ci.org/juampynr/d8cidemo
