#!/usr/bin/env bash
# Run Unit and Kernel tests. Generate coverage report.
cp .travis/config/phpunit.xml web/core/
cd web
../vendor/bin/phpunit -c core --debug --coverage-clover ../build/logs/clover.xml --verbose modules/custom
cd ..
