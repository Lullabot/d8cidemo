#!/usr/bin/env bash
vendor/bin/phpcs --config-set installed_paths vendor/drupal/coder/coder_sniffer
vendor/bin/phpcs --standard=Drupal web/modules/custom
vendor/bin/phpcs --standard=DrupalPractice web/modules/custom
